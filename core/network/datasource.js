const axios = require('axios');

exports.get = async (uri, headers) => {
    return axios.get(uri, {
        headers: headers,
    });
};

exports.getStream = async (uri, headers) => {
    return axios.get(uri, {
        headers: headers,
    });
};

var params = {
    endpoint: "entries",
    language: "en-us",
    words: "silent",
}

var headers = {
    'app_id': "194e0a06",
    'app_key': "411805b9b481e61b6920b0c4c11591a3"
}

axios.get("https://od-api.oxforddictionaries.com/api/v2/", {
        params,
        headers
    }).then(res => {
        console.log(res.data.representation);
    }).catch((err) => console.log(err))
    .finally(() => console.log('done'));