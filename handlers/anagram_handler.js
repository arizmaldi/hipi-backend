const Joi = require('@hapi/joi');
const Services = require('../services');

const internals = {};

internals.anagramConfig = {
    description: 'Check Anagram',
    notes: 'Returns Anagram',
    tags: ['api'],
    handler: (request, h) => anagramHandler(request, h),
    validate: {
        query: Joi.object({
            word1: Joi.string().min(1).required(),
            word2: Joi.string().min(1).required()
        })

    }
};

const anagramHandler = async (request, h) => {
    const isValid =
        (await Services.WordService.isValidWord(request.query.word1)) &&
        (await Services.WordService.isValidWord(request.query.word2));
    return isValid ?
        Services.AnagramService.isAnagram(request.query.word1, request.query.word2) :
        false;
};

module.exports = internals;