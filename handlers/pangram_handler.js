const {getOnePangram, getAllPangram} = require('../repository');

const internal = {};

internal.randomPangramConfig = {
    handler: (response, h) => pangramRandomHandler(response, h),
    description: 'Get Random Pangram',
    notes: 'Returns Random Pangram',
    tags: ['api'],
};

const pangramRandomHandler = async (response, h) => {
    return await getOnePangram()
        .catch(err => {
            throw err;
        });
};

internal.listPangramConfig = {
    handler: (response, h) => listPangramHandler(response, h),
    description: 'Get All Pangram',
    notes: 'Returns All Pangram',
    tags: ['api'],
};

const listPangramHandler = async (response, h) => {
    return await getAllPangram()
        .catch(err => {
            throw err;
        });
};

module.exports = internal;