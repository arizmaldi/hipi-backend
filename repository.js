const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://training:training@clusterjs-yhrgy.gcp.mongodb.net/test?retryWrites=true&w=majority";
const db = 'flutter_pangram';
const collectionPangram = 'data_book';
const collectionAnagram = 'anagram';


const client = new MongoClient(uri, {
    useNewUrlParser: true,
    haInterval: 500,
    useUnifiedTopology: true
});

exports.startMongo = async () => {
    await checkDBConnection();
};

exports.getOnePangram = async () => {
    const newCL = await checkDBConnection();
    return await newCL.db(db).collection(collectionPangram)
        .aggregate([{
            $sample: {
                size: 1
            }
        }])
        .toArray()
        .then((data) => randomOneData(data[0]))
        .catch((err) => {
            console.log(err);
            throw err;
        });
};

randomOneData = (datas) => {
    const pangrams = datas.pangram;
    const num = Math.floor(Math.random() * (pangrams.length - 1));
    datas.pangram = pangrams[num];
    return datas;
};

exports.getAllPangram = async () => {
    const newCL = await checkDBConnection();
    return await newCL.db(db).collection(collectionPangram)
        .find({})
        .toArray()
        .catch((err) => {
            console.log(err);
            throw err;
        });
};

exports.addNewWord = async (data) => {
    const newCL = await checkDBConnection();
    return await newCL.db(db).collection(collectionAnagram)
        .updateOne({
            word: data.word
        }, {
            $set: data
        }, {
            upsert: true
        }).then(data => data);
};

exports.helloAsync = async (name) => {
    return new Promise(resolve => resolve('Hello ' + name));
};

const checkDBConnection = async () => {
    if (!client.isConnected()) {
        await client.connect().catch(err => {
            console.log('error while checking DB connection: ');
            throw err;
        });
    }
    return client;

};