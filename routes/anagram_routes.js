const handlers = require('../handlers');

const internal = {};

internal.route = [{
    method: 'GET',
    path: '/anagram',
    options: handlers.AnagramHandler.anagramConfig
}];

module.exports = internal.route;