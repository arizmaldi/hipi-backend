const handlers = require('../handlers');

const internals = {};

internals.routes = [{
        method: 'GET',
        path: '/pangram',
        options: handlers.PangramHandler.randomPangramConfig
    },
    {
        method: 'GET',
        path: '/list',
        options: handlers.PangramHandler.listPangramConfig
    },
];

module.exports = internals.routes;