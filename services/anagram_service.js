const internals = {};

internals.isAnagram = (word1, word2) => {
    if (word1.length !== word2.length)
        return false;

    const chars1 = [...word1.toLowerCase()]
        .sort(ascendingSort);

    const chars2 = [...word2.toLowerCase()]
        .sort(ascendingSort);

    const isSame = chars1
        .map((val, i) => chars2[i] === val)
        .every(val = val);

    return isSame;
};

const ascendingSort = (a, b) => a.charCodeAt(0) - b.charCodeAt(0);

module.exports = internals;