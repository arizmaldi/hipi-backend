const network = require('../core/network/datasource');
const readline = require('readline');
const repository = require('../repository');

const internals = {};

internals.isValidWord = async (word) => {
    return !((await network.get(`http://www.anagramica.com/lookup/${word}`)).data.found === 0);
};

internals.getLines = async (stream) => {
    const lineStream = readline.createInterface({
        input: stream,
        console: false
    });

    lineStream.on('line', async (str) => {
        const isValid = await this.isValidWord(str);

        if (isValid) {
            const word = {
                'word': str,
                'sorted': this.sort(str)
            };
            repository.addNewWord(word).then(() => console.log('data masuk'));
        }
    });
};

const ascendingSort = (a, b) => a.charCodeAt(0) - b.charCodeAt(0);

internals.sort = (str) => {
    return [...str.toLowerCase()]
        .sort(ascendingSort)
        .reduce((a, b) => a + b);
};

module.exports = internals;