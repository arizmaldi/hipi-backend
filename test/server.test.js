const Lab = require('@hapi/lab');
const {
    expect
} = require('@hapi/code');
const {
    afterEach,
    beforeEach,
    describe,
    it
} = exports.lab = Lab.script();
const {
    init
} = require('../server');

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200', {
        timeout: 4000
    }, async () => {
        const res = await server.inject({
            method: 'get',
            url: '/pangram'
        });
        expect(res.statusCode).to.equal(200);
    });

    it('should list pangram successfully', {
        timeout: 4000
    }, async function () {
        const data = await server.inject('/list');
        expect(data.statusCode).to.equal(200);
        expect(data.result.length).to.equal(3);
    });

    // it('should return 1 pangram successfully', async function () {
    //     const data = await server.inject('/pangram');
    //     expect(data.statusCode).to.equal(200);
    //     expect(data.result.length).to.equal(1);
    // });
});